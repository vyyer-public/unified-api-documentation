# Unified API Documentation

Unified API deployment contains a number of different APIs that you can interact with. They include:

- [Domains API](Domains.md) - Manage API keys and domains they can be used with to work with some APIs.
- [Licenses API](Licenses.md) - Manage Microblink licenses that can be used from web, iOS or Android Vyyer SDKs to perform ID scans through in-built Microblink SDK.
- [Settings API](Settings.md) - Manage and configure Unified API through general and user-based settings.
- [Workflow Use Cases](WorkflowUseCases.md) - Identify and understand how to work with Workflows through various use cases.
- [Workflows API](Workflows.md) - Manage and work with workflows to run the processes that verify the customer.
- [Workflow Templates API](WorkflowTemplates.md) - Manage and create custom workflow templates to navigate your customers through for validation.
- [Identities API](Identities.md) - Manage the identities of customers that were validated through Workflows API.
