# Introduction

Domains API has been created to manage the list of domains and API keys associated with them.
Each API key bound to the specified domain can only be used from it (i.e. the domain must be in the Origin header).

# Authentication

Vyyer APIs have two methods of authentication - Bearer token, or an API key.
First one is obtained from Auth0
using [Password Credentials](https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow),
while the second is issued within Unified API for specified domain.
The API key requests are generally limited to non-administrative API methods.

**To work with Domains API you will need to authenticate and obtain a bearer token.**

# Usage

## Creating a domain

You can create a domain/API pair by making the following request:

``` bash
curl --location 'https://${Domain}/api/v2/domains/create' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}' \
--data '{
    "Name": "${YourDomain}",
    "IPAddress": "${YourIPAddress}",
    "APIKey": "${APIKey}"
}'
```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication
- **${YourDomain}** - Your fully-qualified domain name where the API key will be used from
- **${YourIPAddress}** - The IP address of the domain that API key is going to be used from
- **${APIKey}** - A unique API key string that you will use to authorize the calls with (we recommend using a random MD5)

## Updating a domain by ID

You can update a domain/API pair by making the following request:

``` bash
curl --location 'https://${Domain}/api/v2/domains/${ID}' --request PUT \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}' \
--data '{
    "Name": "${YourDomain}",
    "IPAddress": "${YourIPAddress}",
    "APIKey": "${APIKey}"
}'
```

- **${Domain}** - Domain of your Unified API deployment
- **${ID}** - The unique domain record identifier
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication
- **${YourDomain}** - Your fully-qualified domain name where the API key will be used from
- **${YourIPAddress}** - The IP address of the domain that API key is going to be used from
- **${APIKey}** - A unique API key string that you will use to authorize the calls with (we recommend using a random MD5)


## Fetching domain by ID

To fetch a domain by its unique identifier (ID), send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/domains/${ID}' \
--header 'Authorization: Bearer ${BearerToken}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication
- **${ID}** - The unique domain record identifier

The response will look like this:

``` json
{
    "ID": 1,
    "Name": "https://your.domain.com",
    "IPAddress": "127.0.0.1",
    "APIKey": "9b6338906c860715a10d791391a129ae",
    "UserID": 1,
    "CreatedAt": "2023-07-13 12:27:42",
    "UpdatedAt": "2023-07-13 12:27:42"
}
 ```

## Fetching domains list

To fetch all existing domains, send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/domains/get' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
  "Data": [
    {
      "ID": 17,
      "Name": "41032 (v5, Android, id.vyyer)",
      "Platform": "android",
      "Package": "id.vyyer",
      "Version": 5,
      "Number": 41032,
      "Value": "sRwAAAAIaWQudnl5ZXIRwYuCczw07eWpYGYk8La32QNREpVgvRSS7P0U0KngXGxJ/Z2u0RMYPdJr99pdx50kEF7jgsUNAJYeqs7bs+ejPfGyDLh+2QhilmbpDqmFGrQakBDOAMGqpsTCsaSYauZ5kcHLcSVEszJ6EPqSTTABAOvzQbLeOnlPw2ugr+Fqnip3lRJwfmN+Y8BPi3eAQFg47GrtQdDdRY1llazHv7ivDdTS0om2TLOMFnZTiOmt",
      "ExpiresAt": "2023-09-19 23:59:59"
    }
  ]
}
 ```

## Deleting domain by ID

To delete a domain by its unique identifier (ID), send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/domains/${ID}' --request DELETE \
--header 'Authorization: Bearer ${BearerToken}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication
- **${ID}** - The unique domain record identifier

The response will look like this:

``` json
{
    "Errors" : []
}
 ```

## Domains API Information

You can obtain Domains API overview information by making the following request.
It will contain the min/max. values for IDs, the min/max. dates for their creation,
the database and code version, as well as any errors:

``` bash
curl --location 'https://${Domain}/api/v2/domains/info/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "IDs": [1,2,3,4,5],
    "Dates": ["", ""],
    "Count": 5,
    "DatabaseVersion": "33kfk4jdsf9",
    "CodeVersion": "",
    "APIVersion": "2.0.0",
    "Errors": []
}
 ```

## Domains API Metadata

You can obtain Domains API metadata in Swagger' JSON format by making the following request:

**You don't need Bearer token to make this request.**

``` bash
curl --location 'https://${Domain}/api/v2/domains/meta/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "openapi": "3.0.0",
    "info": {
        "title": "Domain management",
        "description": "Unified API to manage domains",
        "termsOfService": "https://swagger.io/terms/",
        "contact": {
            "name": "Administrator",
            "email": "admin@vyyer.id"
        },
        "domain": {
            "name": "Private"
        },
        "version": "2.0.0"
    },
    "servers": [
        {
            "url": "https://${Domain}/api/v2/domains",
            "description": "Domains API"
        }
    ],
    "paths": {
        "/meta/": {
            "get": {
                "tags": [
                    "meta"
                ],
                "summary": "Retrieves meta information about domains",
                "description": "Retrieves meta information about domains",
                "operationId": "Meta",
                "responses": {
                    "200": {
                        "description": "Response with domains meta details"
                    },
                    "400": {
                        "description": "Bad Request"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    }
                },
                "security": [
                    {
                        "domain_auth": [
                            "read:domain-meta"
                        ]
                    }
                ]
            }
        },
        
        ... other methods
}
 ```