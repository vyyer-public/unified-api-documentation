# Introduction

Here you will find most common use cases for Workflows API.

## Creating one-time link for QR code
QR code contains a list of workflow stages that were created during workflow instantiation.
Each workflow stage will have a name (Scan Front, Scan Back etc), and Session unique identifier for that stage.

For example, if you created "ID Verification" workflow instance, you will see the following URL encoded in QR code:

``
https://websdk.vyyer.id/test?Scan Back=5kgj221g-ca9a-4dbf-fde1-41fe92af150f&Scan Front=78d1657c-t6k1-k331-b94f-1992ebf2d681
``

In order to generate such URL, you need to create workflow via [createAndRegister](Workflows.md#creating-a-workflow) method, 
and then collect each entry' Name and SessionUID in Data array, putting them into the URL.

## Checking verification results

To check verification results by UserUID or SessionUID, send a [request](Workflows.md#fetching-workflow-by-unique-identifier) to Workflows API with it:

```bash
curl --location --request GET 'https://${Domain}/api/v2/workflows/id/${User-or-Session-UID}APIKey=${APIKey}&Children=true' \
--header 'Origin: ${APIKeyDomain}' \
--header 'Content-Type: application/json'
```

- **${Domain}** - Domain of your Unified API deployment
- **${User-or-Session-UID}** - The unique workflow session identifier
- **${APIKey}** - API key associated with the Origin domain that request is coming from
- **${APIKeyDomain}** - Fully-qualified domain name where the API key is used from

## Receive a callback
