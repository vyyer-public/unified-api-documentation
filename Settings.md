# Introduction

Settings API has been created to manage the list of user and/or general settings.
Settings control the way Unified API operates, and include parameters such as adult age,
maximum workflow attempts, and others.

# Authentication

Vyyer APIs have two methods of authentication - Bearer token, or an API key.
First one is obtained from Auth0
using [Password Credentials](https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow),
while the second is issued within Unified API for specified domain.
The API key requests are generally limited to non-administrative API methods.

**To work with Settings API you will need to authenticate and obtain a bearer token.**

# Usage

## Creating a setting

You may not create a setting record as they are created during Unified API initialization.

## Fetching user settings list

To retrieve a list of user-based settings, send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/settings/user' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
    "Data": [
    {... User Setting 1},
    {... User Setting 2}
    ],
    "Errors": []
}
 ```

## Fetching general settings list

To retrieve a list of general settings, send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/settings/general' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
    "Data": [
    {... General Setting 1},
    {... General Setting 2}
    ],
    "Errors": []
}
 ```

## Updating user setting by its name

To update a user-based setting, send the following request:

``` bash
curl --request PUT --location 'https://${Domain}/api/v2/settings/user' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken} \
--data '{
    "Data" : [
         {
            "Name": "User Setting Name #1",
            "Type": "string",
            "Value": "abcdefg"
         }
    ]
}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
    "Data": 
    [
         {
            "Name": "User Setting Name #1",
            "Type": "string",
            "Value": "abcdefg"
         }
    ],
    "Errors": []
}
 ```

## Updating general setting by its name

To update general setting, send the following request:

``` bash
curl --request PUT --location 'https://${Domain}/api/v2/settings/general' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken} \
--data '{
    "Data" : [
         {
            "Name": "General Setting Name #1",
            "Type": "maxAttempts",
            "Value": "5"
         }
    ]
}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
    "Data": 
    [
         {
            "Name": "General Setting Name #1",
            "Type": "maxAttempts",
            "Value": "5"
         }
    ],
    "Errors": []
}
 ```

## Settings API Information

You can obtain Settings API overview information by making the following request.
It will contain the min/max. values for IDs, the min/max. dates for their creation,
the database and code version, as well as any errors:

``` bash
curl --location 'https://${Domain}/api/v2/settings/info/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "IDs": [1,2,3,4,5],
    "Dates": ["", ""],
    "Count": 5,
    "DatabaseVersion": "53933k3csd93",
    "CodeVersion": "",
    "APIVersion": "2.0.0",
    "Errors": []
}
 ```

## Settings API Metadata

You can obtain Settings API metadata in Swagger' JSON format by making the following request:

**You don't need Bearer token to make this request.**

``` bash
curl --location 'https://${Domain}/api/v2/settings/meta/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "openapi": "3.0.0",
    "info": {
        "title": "Setting management",
        "description": "Unified API to manage settings",
        "termsOfService": "https://swagger.io/terms/",
        "contact": {
            "name": "Administrator",
            "email": "admin@vyyer.id"
        },
        "setting": {
            "name": "Private"
        },
        "version": "2.0.0"
    },
    "servers": [
        {
            "url": "https://${Domain}/api/v2/settings",
            "description": "Settings API"
        }
    ],
    "paths": {
        "/meta/": {
            "get": {
                "tags": [
                    "meta"
                ],
                "summary": "Retrieves meta information about settings",
                "description": "Retrieves meta information about settings",
                "operationId": "Meta",
                "responses": {
                    "200": {
                        "description": "Response with settings meta details"
                    },
                    "400": {
                        "description": "Bad Request"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    }
                },
                "security": [
                    {
                        "setting_auth": [
                            "read:setting-meta"
                        ]
                    }
                ]
            }
        },
        
        ... other methods
}
 ```