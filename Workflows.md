# Introduction

Workflows API has been created to operate with workflows that are created to guide customers through
the process of identity verification.

# Authentication

Vyyer APIs have two methods of authentication - Bearer token, or an API key.
First one is obtained from Auth0
using [Password Credentials](https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow),
while the second is issued within Unified API for specified domain.
The API key requests are generally limited to non-administrative API methods.

**To work with Workflows API you will need to authenticate and obtain a bearer token, OR use an API key**

# Usage

Workflows are created to guide you through the steps of verifying the person via:

- ID Scan & verification
- Passport scan & verification
- Matching selfie to ID/Passport photo (face matching)
- Cross-checking data from the from of ID to the back
- Cross-checking data from Passport chip to data from its scan
- Liveness check of the selfie

Above can be configured in the settings section of the dashboard, 
and a new workflow can be created with the combination of the above steps.

## Creating a workflow

To create the workflow you need to send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/workflows/createAndRegister?APIKey=${APIKey}' \
--header 'Origin: ${APIKeyDomain}' \
--header 'Content-Type: application/json' \
--data '{
    "Name": "${WorkflowName}",
    "UserUID": "${UserUID}"
}'
```

- **${Domain}** - Domain of your Unified API deployment
- **${APIKey}** - API key associated with the Origin domain that request is coming from
- **${APIKeyDomain}** - Fully-qualified domain name where the API key is used from
- **${UserUID}** - A unique optional key that is associated with the person being verified
- **${WorkflowName}** - Optional name parameter of workflow

The response will look like this:
```json
{
    "Errors": [],
    "SessionUID": "3059931-89aw-gt92-9110-0e20bd5676a2",
    "UserUID": "${UserUID}",
    "IdentityUID": "",
    "Attempt": 1,
    "MaxAttempts": 5,
    "Name": "Back ID Verification",
    "Identity": null,
    "State": 2,
    "Verdict": -1,
    "StateName": "Registered",
    "VerdictName": "Incomplete",
    "FinalVerdictName": "Incomplete",
    "Data": [
        {
            "ScanID": 0,
            "IdentityID": 0,
            "Attempts": [
                {
                    "ScanID": 0,
                    "IdentityID": 0,
                    "Verdict": "Incomplete",
                    "Description": "Incomplete",
                    "Attempt": 1
                }
            ],
            "UID": "f374b312-d0ae-42da-8f0b-fede8859c81a",
            "Type": 2,
            "Name": "Scan Back",
            "State": "Registered",
            "ClientCallbackURL": "",
            "ServerCallbackURL": "k",
            "IFRAMEURL": "https://vyyer.id/idscan",
            "APIURL": "APP_URL",
            "Data": [
                {
                    "Key": "Verdict",
                    "Value": "Incomplete",
                    "Type": "string"
                }
            ],
            "Attempt": 1,
            "Images": [],
            "ExpiresAt": "2023-07-23 16:24:00",
            "CreatedAt": "2023-07-23 16:14:33",
            "UpdatedAt": "2023-07-23 16:14:33",
            "Verdict": "Incomplete",
            "Description": "Incomplete"
        }
    ],
    "CreatedAt": "2023-07-23 16:14:33",
    "UpdatedAt": "2023-07-23 16:14:33",
    "Auth0UserID": "auth0|64ac470cae098e4df834ee7e",
    "Auth0OrgID": "org_3sHWFIUocbMuW4zb",
    "OneTimeToken": "",
    "FullName": "",
    "DateOfBirth": "",
    "Gender": "",
    "IDNumber": "",
    "DateOfIssue": "",
    "DateOfExpiry": "",
    "AddressStreet": "",
    "AddressCityState": "",
    "AddressZIP": "",
    "AddressCountry": "",
    "DomainOrPackage": "https://vt.devyy.io"
}
```

After creating the workflow you can go through the steps to complete person's identity. Examples of verification will be provided in [Use Cases](WorkflowUseCases.md) 

## Fetching workflows list

```bash
curl --location --request POST 'https://${Domain}/workflows/get?APIKey=${APIKey}&Children=true' \
--header 'Origin: ${APIKeyDomain}' \
--header 'Content-Type: application/json' \
--data '{
    "Children": 1,
    "OrderBy": [
        "CreatedAt 0"
    ],
    "ClientName": "",
    "Page": 1,
    "PerPage": 10
}'
```

- **${Domain}** - Domain of your Unified API deployment
- **${SessionUID}** - The unique workflow record identifier
- **${APIKey}** - API key associated with the Origin domain that request is coming from
- **${APIKeyDomain}** - Fully-qualified domain name where the API key is used from

The response will look like this:

```json
{
    "Data" : [
      {...Workflow 1 Data}, 
      {...Workflow 2 Data}
    ],
    "Errors": [],
    "Pagination": {
        "CurrentPage": 1,
        "PerPage": 10,
        "Count": 137,
        "Pages": 14
    }
}
```

## Fetching workflow by unique identifier

You can retrieve workflow by its unique identifier that is stored in SessionUID by making a request:

```bash
curl --location --request GET 'https://${Domain}/workflows/id/${SessionUID}?APIKey=${APIKey}&Children=true' \
--header 'Origin: ${APIKeyDomain}' \
--header 'Content-Type: application/json'
```

- **${Domain}** - Domain of your Unified API deployment
- **${SessionUID}** - The unique workflow session identifier
- **${APIKey}** - API key associated with the Origin domain that request is coming from
- **${APIKeyDomain}** - Fully-qualified domain name where the API key is used from

The response will look like this:

```json
{
    "Errors": [],
    "SessionUID": "341f2176-85a0-4d30-b9cc-0e20bd5676a2",
    "UserUID": "${UserUID}",
    "IdentityUID": "",
    "Attempt": 1,
    "MaxAttempts": 5,
    "Name": "Back ID Verification",
    "Identity": null,
    "State": 2,
    "Verdict": -1,
    "StateName": "Registered",
    "VerdictName": "Incomplete",
    "FinalVerdictName": "Incomplete",
    "Data": [
        {
            "ScanID": 0,
            "IdentityID": 0,
            "Attempts": [
                {
                    "ScanID": 0,
                    "IdentityID": 0,
                    "FullName": "",
                    "DateOfBirth": "",
                    "Gender": "",
                    "IDNumber": "",
                    "DateOfIssue": "",
                    "DateOfExpiry": "",
                    "AddressStreet": "",
                    "AddressCityState": "",
                    "AddressZIP": "",
                    "AddressCountry": "",
                    "Verdict": "Incomplete",
                    "Description": "Incomplete",
                    "Attempt": 1
                }
            ],
            "UID": "f374b312-d0ae-42da-8f0b-fede8859c81a",
            "Type": 2,
            "Name": "Scan Back",
            "State": "Registered",
            "ClientCallbackURL": "",
            "ServerCallbackURL": "",
            "IFRAMEURL": "https://vyyer.id/idscan",
            "APIURL": "APP_URL",
            "Data": [
                {
                    "Key": "Verdict",
                    "Value": "Incomplete",
                    "Type": "string"
                }
            ],
            "Attempt": 1,
            "Images": [],
            "ExpiresAt": "2023-07-23 16:24:00",
            "CreatedAt": "2023-07-23 16:14:33",
            "UpdatedAt": "2023-07-23 16:14:33",
            "Verdict": "Incomplete",
            "Description": "Incomplete"
        }
    ],
    "CreatedAt": "2023-07-23 16:14:33",
    "UpdatedAt": "2023-07-23 16:14:33",
    "Auth0UserID": "auth0|64ac470cae098e4df834ee7e",
    "Auth0OrgID": "org_3sHWFIUocbMuW4zb",
    "DomainOrPackage": ""
}
```

## Initiating next attempt

When working with the workflow steps, you will need to switch to the next step. 
This will occur when current step' check has failed and next attempt has to be initiated.
Each workflow step may have up to N attempts (this is configured in Settings section of the Dashboard).
When maximum number of attempts for workflow step has been reached, the workflow step can no longer be worked with.

```bash
curl --location --request PUT 'https://${Domain}/api/v2/workflows/next/${SessionUID}?APIKey=${APIKey}' \
--header 'Origin: ${APIKeyDomain}'
```

- **${Domain}** - Domain of your Unified API deployment
- **${SessionUID}** - The unique workflow record identifier
- **${APIKey}** - API key associated with the Origin domain that request is coming from
- **${APIKeyDomain}** - Fully-qualified domain name where the API key is used from

The response will look like this:
```json
{
    "Errors": [],
    "SessionUID": "c27c134b-a8f0-4bdb-b36a-2991142d28a8",
    "ClientUID": "",
    "IdentityUID": "",
    "Attempt": 2,
    "MaxAttempts": 5,
    "Name": "Back ID Verification",
    "Identity": null,
    "State": 2,
    "Verdict": -1,
    "StateName": "Registered",
    "VerdictName": "Incomplete",
    "FinalVerdictName": "Incomplete",
    "Data": [
        {
            "ScanID": 0,
            "IdentityID": 0,
            "Attempts": [
                {
                    "ScanID": 0,
                    "IdentityID": 0,
                    "FullName": "",
                    "DateOfBirth": "",
                    "Gender": "",
                    "IDNumber": "",
                    "DateOfIssue": "",
                    "DateOfExpiry": "",
                    "AddressStreet": "",
                    "AddressCityState": "",
                    "AddressZIP": "",
                    "AddressCountry": "",
                    "Verdict": "Incomplete",
                    "Description": "Incomplete",
                    "Attempt": 1
                },
                {
                    "ScanID": 0,
                    "IdentityID": 0,
                    "FullName": "",
                    "DateOfBirth": "",
                    "Gender": "",
                    "IDNumber": "",
                    "DateOfIssue": "",
                    "DateOfExpiry": "",
                    "AddressStreet": "",
                    "AddressCityState": "",
                    "AddressZIP": "",
                    "AddressCountry": "",
                    "Verdict": "Incomplete",
                    "Description": "Incomplete",
                    "Attempt": 2
                }
            ],
            "UID": "c61e9baa-fd50-4b92-98b7-415423b9d556",
            "Type": 2,
            "Name": "Scan Back",
            "State": "Registered",
            "ClientCallbackURL": "",
            "ServerCallbackURL": "",
            "IFRAMEURL": "https://vyyer.id/idscan",
            "APIURL": "APP_URL",
            "Data": [
                {
                    "Key": "Verdict",
                    "Value": "Incomplete",
                    "Type": "string"
                }
            ],
            "Attempt": 2,
            "Images": [],
            "ExpiresAt": "2023-07-23 16:40:00",
            "CreatedAt": "2023-07-23 16:30:22",
            "UpdatedAt": "2023-07-23 16:30:35",
            "Verdict": "Incomplete",
            "Description": "Incomplete"
        }
    ],
    "CreatedAt": "2023-07-23 16:30:22",
    "UpdatedAt": "2023-07-23 16:30:35",
    "Auth0UserID": "auth0|64ac470cae098e4df834ee7e",
    "Auth0OrgID": "org_3sHWFIUocbMuW4zb",
    "OneTimeToken": "",
    "FullName": "",
    "DateOfBirth": "",
    "Gender": "",
    "IDNumber": "",
    "DateOfIssue": "",
    "DateOfExpiry": "",
    "AddressStreet": "",
    "AddressCityState": "",
    "AddressZIP": "",
    "AddressCountry": "",
    "DomainOrPackage": "https://vt.devyy.io"
}
```


## Workflows API Information

You can obtain Workflows API overview information by making the following request.
It will contain the min/max. values for IDs, the min/max. dates for their creation,
the database and code version, as well as any errors:

``` bash
curl --location 'https://${Domain}/api/v2/workflows/info/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "IDs": [1,2,3,4,5],
    "Dates": ["", ""],
    "Count": 5,
    "DatabaseVersion": "53933k3csd93",
    "CodeVersion": "",
    "APIVersion": "2.0.0",
    "Errors": []
}
 ```

## Workflows API Metadata

You can obtain Workflows API metadata in Swagger' JSON format by making the following request:

**You don't need Bearer token to make this request.**

``` bash
curl --location 'https://${Domain}/api/v2/workflows/meta/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "openapi": "3.0.0",
    "info": {
        "title": "Workflow management",
        "description": "Unified API to manage workflows",
        "termsOfService": "https://swagger.io/terms/",
        "contact": {
            "name": "Administrator",
            "email": "admin@vyyer.id"
        },
        "workflow": {
            "name": "Private"
        },
        "version": "2.0.0"
    },
    "servers": [
        {
            "url": "https://${Domain}/api/v2/workflows",
            "description": "Workflows API"
        }
    ],
    "paths": {
        "/meta/": {
            "get": {
                "tags": [
                    "meta"
                ],
                "summary": "Retrieves meta information about workflows",
                "description": "Retrieves meta information about workflows",
                "operationId": "Meta",
                "responses": {
                    "200": {
                        "description": "Response with workflows meta details"
                    },
                    "400": {
                        "description": "Bad Request"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    }
                },
                "security": [
                    {
                        "workflow_auth": [
                            "read:workflow-meta"
                        ]
                    }
                ]
            }
        },
        
        ... other methods
}
 ```