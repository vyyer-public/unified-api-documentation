# Introduction

Licenses API has been created to manage the list of Microblink licenses that are used by mobile devices to scan driver
licenses, passports and other IDs.  
License is restricted by Microblink by the domain or package name it's being used from, and has an expiration date.

# Authentication

Vyyer APIs have two methods of authentication - Bearer token, or an API key.
First one is obtained from Auth0
using [Password Credentials](https://auth0.com/docs/get-started/authentication-and-authorization-flow/resource-owner-password-flow),
while the second is issued within Unified API for specified domain.
The API key requests are generally limited to non-administrative API methods.

**To work with Licenses API you will need to authenticate and obtain a bearer token.**

# Usage

## Creating a license

You may not create a license record as they are synchronized regularly from the centralized location.

## Fetching license by name, platform or other means

To search licenses, send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/licenses/search' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}' \
--data '{
    "ID" : ${ID},
    "Name" : "${Name]",
    "Platform" : "${Platform}",
    "Package" : "${Package}",
    "Version" : ${Version},
    "Number" : ${Number}
}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication
- **${ID}** - The unique license record identifier
- **${Platform}** - android, ios or web value
- **${Package}** - License package value
- **${Version}** - Version of the license (5, 6 or above) issued by Microblink
- **${Number}** - License number issued by Microblink

The response will look like this:

``` json
{
  "Data": [
    {
      "ID": 17,
      "Name": "41032 (v5, Android, id.vyyer)",
      "Platform": "android",
      "Package": "id.vyyer",
      "Version": 5,
      "Number": 41032,
      "Value": "sRwAAAAIaWQudnl5ZXIRwYuCczw07eWpYGYk8La32QNREpVgvRSS7P0U0KngXGxJ/Z2u0RMYPdJr99pdx50kEF7jgsUNAJYeqs7bs+ejPfGyDLh+2QhilmbpDqmFGrQakBDOAMGqpsTCsaSYauZ5kcHLcSVEszJ6EPqSTTABAOvzQbLeOnlPw2ugr+Fqnip3lRJwfmN+Y8BPi3eAQFg47GrtQdDdRY1llazHv7ivDdTS0om2TLOMFnZTiOmt",
      "ExpiresAt": "2023-09-19 23:59:59"
    }
  ]
}
 ```

## Fetching licenses list

To fetch all existing licenses, send the following request:

``` bash
curl --location 'https://${Domain}/api/v2/licenses/get' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ${BearerToken}' \
--data '{}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
  "Data": [
    {
      "ID": 17,
      "Name": "41032 (v5, Android, id.vyyer)",
      "Platform": "android",
      "Package": "id.vyyer",
      "Version": 5,
      "Number": 41032,
      "Value": "sRwAAAAIaWQudnl5ZXIRwYuCczw07eWpYGYk8La32QNREpVgvRSS7P0U0KngXGxJ/Z2u0RMYPdJr99pdx50kEF7jgsUNAJYeqs7bs+ejPfGyDLh+2QhilmbpDqmFGrQakBDOAMGqpsTCsaSYauZ5kcHLcSVEszJ6EPqSTTABAOvzQbLeOnlPw2ugr+Fqnip3lRJwfmN+Y8BPi3eAQFg47GrtQdDdRY1llazHv7ivDdTS0om2TLOMFnZTiOmt",
      "ExpiresAt": "2023-09-19 23:59:59"
    }
  ]
}
 ```

## Searching for license(s)

To filter the licenses, you need to make the request
with one or many of the following parameters in the body:

``` bash
curl --location 'https://${Domain}/api/v2/licenses/search' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer ...' \
--data '{
    "ID" : 1,
    "Platform" : "android",
    "Package" : "id.vyyer",
    "Version" : 6,
    "Number" : 15051,
    "ExpiresAt" : "2028-07-23"
}'
 ```

- **${Domain}** - Domain of your Unified API deployment
- **${BearerToken}** - Bearer token received from Auth0 through client-credentials or password authentication

The response will look like this:

``` json
{
  "Data": [
    {
      "ID": 17,
      "Name": "41032 (v5, Android, id.vyyer)",
      "Platform": "android",
      "Package": "id.vyyer",
      "Version": 5,
      "Number": 41032,
      "Value": "sRwAAAAIaWQudnl5ZXIRwYuCczw07eWpYGYk8La32QNREpVgvRSS7P0U0KngXGxJ/Z2u0RMYPdJr99pdx50kEF7jgsUNAJYeqs7bs+ejPfGyDLh+2QhilmbpDqmFGrQakBDOAMGqpsTCsaSYauZ5kcHLcSVEszJ6EPqSTTABAOvzQbLeOnlPw2ugr+Fqnip3lRJwfmN+Y8BPi3eAQFg47GrtQdDdRY1llazHv7ivDdTS0om2TLOMFnZTiOmt",
      "ExpiresAt": "2023-09-19 23:59:59"
    }
  ]
}
 ```

## Licenses API Information

You can obtain Licenses API overview information by making the following request.
It will contain the min/max. values for IDs, the min/max. dates for their creation,
the database and code version, as well as any errors:

``` bash
curl --location 'https://${Domain}/api/v2/licenses/info/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "IDs": [1,2,3,4,5],
    "Dates": ["", ""],
    "Count": 5,
    "DatabaseVersion": "53933k3csd93",
    "CodeVersion": "",
    "APIVersion": "2.0.0",
    "Errors": []
}
 ```

## Licenses API Metadata

You can obtain Licenses API metadata in Swagger' JSON format by making the following request:

**You don't need Bearer token to make this request.**

``` bash
curl --location 'https://${Domain}/api/v2/licenses/meta/' \
--header 'Content-Type: application/json'
 ```

- **${Domain}** - Domain of your Unified API deployment

The response will look like this:

``` json
{
    "openapi": "3.0.0",
    "info": {
        "title": "License management",
        "description": "Unified API to manage licenses",
        "termsOfService": "https://swagger.io/terms/",
        "contact": {
            "name": "Administrator",
            "email": "admin@vyyer.id"
        },
        "license": {
            "name": "Private"
        },
        "version": "2.0.0"
    },
    "servers": [
        {
            "url": "https://${Domain}/api/v2/licenses",
            "description": "Licenses API"
        }
    ],
    "paths": {
        "/meta/": {
            "get": {
                "tags": [
                    "meta"
                ],
                "summary": "Retrieves meta information about licenses",
                "description": "Retrieves meta information about licenses",
                "operationId": "Meta",
                "responses": {
                    "200": {
                        "description": "Response with licenses meta details"
                    },
                    "400": {
                        "description": "Bad Request"
                    },
                    "401": {
                        "description": "Unauthorized"
                    },
                    "403": {
                        "description": "Forbidden"
                    }
                },
                "security": [
                    {
                        "license_auth": [
                            "read:license-meta"
                        ]
                    }
                ]
            }
        },
        
        ... other methods
}
 ```